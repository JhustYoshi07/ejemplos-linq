
using System;
using System.Collections.Generic;
using System.Linq;

public class Ejemplo_LINQ_2
{
   //Jhustin Ismael Arias Perez 4-A T/M

public static void Main(string[] args)
{
    var startingDeck = from s in Suits()
                       from r in Ranks()
                       select new { Suit = s, Rank = r };

    foreach (var c in startingDeck)
    {
        Console.WriteLine(c);
    }

    // 52 cards in a deck, so 52 / 2 = 26
    var top = startingDeck.Take(26);
    var bottom = startingDeck.Skip(26);
}
}